package v1

import (
	"context"
	"net/http"

	// "strconv"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"

	"api_gateway/genproto/product"
	l "api_gateway/pkg/logger"
)

// CreateProduct creates product
// @Summary create prodcut api
// @Description this api creates new product
// @Tags Product
// @Accept json
// @Produce json
// @Param product body product.Product true "product"
// @Succes 200 {object} product.Product
// @Router /v1/products/ [post]
// router /v1/products [post]
func (h *handlerV1) CreateProduct(c *gin.Context) {
	var (
		body        product.Product
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ProductService().CreateProduct(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create user", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)
}

// CreateCategory creates user
// @Summary create category api
// @Description this api creates new Category
// @Tags Product
// @Accept json
// @Produce json
// @Param product body product.Category true "product"
// @Succes 200 {object} product.Category
// @Router /v1/categories/ [post]
// router /v1/categories [post]
func (h *handlerV1) CreateCategory(c *gin.Context) {
	var (
		body        product.Category
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ProductService().CreateCategory(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create user", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)
}

// CreateType creates type
// @Summary create Type api
// @Description this api creates new Type
// @Tags Product
// @Accept json
// @Produce json
// @Param product body product.Type true "product"
// @Succes 200 {object} product.Type
// @Router /v1/types/ [post]
// router /v1/type [post]
func (h *handlerV1) CreateType(c *gin.Context) {
	var (
		body        product.Type
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ProductService().CreateType(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create user", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)
}

// GetProductInfoById product
// @Summary GetProductInfoById api
// @Description this api gets product by id
// @Tags Product
// @Accept json
// @Produce json
// @Param id path int true "id"
// @Succes 200 {object} product.Ids
// @Router /v1/get/{id} [get]
// router /v1/products [post]
func (h *handlerV1) GetProductInfoByid(c *gin.Context) {
	var (
		body        product.Ids
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.ProductService().GetProductInfoByid(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create user", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)
}
