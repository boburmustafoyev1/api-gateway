package models

type User struct {
	LastName string
	Name     string
}

type RegisterUsermodel struct {
	LastName string
	Name     string
	UserName string
	Email    string
	Password string
	Code     string
}
