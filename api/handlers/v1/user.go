package v1

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"

	"api_gateway/api/handlers/models"
	"api_gateway/genproto/user"
	"api_gateway/pkg/etc"
	l "api_gateway/pkg/logger"
)

// CreateUser creates user
// @Summary create user api
// @Description this api creates new user
// @Tags User
// @Accept json
// @Produce json
// @Param user body user.User true "User"
// @Succes 200 {object} user.User
// @Router /v1/users/ [post]
// router /v1/users [post]
func (h *handlerV1) CreateUser(c *gin.Context) {
	var (
		body        user.User
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to bind json", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.UserService().CreateUser(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to create user", l.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)
}

// CreateUser creates user
// @Summary get user api
// @Description this api geting
// @Description this api getingDeta user
// @Tags User
// @Accept json
// @Produce json
// @Param id path int true "id"
// @Succes 200 {object} user.User
// @Router /v1/users/{id} [get]
// router /v1/users [get]
func (h *handlerV1) GetUser(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.UserService().GetUserById(
		ctx, &user.GetId{
			Id: id,
		})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get user", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// CreateUser creates user
// @Summary update user api
// @Description this api updating user
// @Tags User
// @Accept json
// @Produce json
// @Param user body user.User true "User"
// @Succes 200 {object} user.User
// @Router /v1/users/update [post]
// router /v1/users [get]
func (h *handlerV1) UpdateUser(c *gin.Context) {
	var body user.User
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.UserService().UpdateUser(ctx, &body)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get user", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// CreateUser creates user
// @Summary delete user api
// @Description this api deleting user
// @Tags User
// @Accept json
// @Produce json
// @Param id path int true "id"
// @Succes 200 {object} user.User
// @Router /v1/users/{id} [delete]
// router /v1/users [delete]
func (h *handlerV1) DeleteUser(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true

	guid := c.Param("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed parse string to int", l.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.UserService().DeleteUser(
		ctx, &user.GetId{
			Id: id,
		})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("failed to get user", l.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

func (h *handlerV1) Register(c *gin.Context) {
	var (
		body models.RegisterUsermodel
	)

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while bind json", l.Error(err))
	}

	body.Email = strings.TrimSpace(body.Email)
	body.UserName = strings.TrimSpace(body.UserName)

	body.Email = strings.ToLower(body.Email)
	body.UserName = strings.ToLower(body.UserName)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	// Username ...
	existsUsername, err := h.serviceManager.UserService().CheckField(ctx, &user.CheckFieldReq{
		Filed: "username",
		Value: body.UserName,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Field check username uniques", l.Error(err))
	}

	if existsUsername.Exists {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
			"info":  "pleace enter another username",
		})
		h.log.Error("this username already exists", l.Error(err))
	}
	// Email ...
	existsEmail, err := h.serviceManager.UserService().CheckField(ctx, &user.CheckFieldReq{
		Filed: "email",
		Value: body.Email,
	})

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Field check email uniques", l.Error(err))
	}

	if existsEmail.Exists {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
			"info":  "pleace enter another email",
		})
		h.log.Error("this email already exists", l.Error(err))
	}

	// Geterate code
	code := etc.GenerateCode(6)
	// send to email/phone
	body.Code = code
	userBodyByte, err := json.Marshal(body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error while marshal user body", l.Error(err))
		return
	}
	err = h.redis.SetWithTTL(body.Email, string(userBodyByte), 300)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("error set to redis body", l.Error(err))
		return
	}

	c.JSON(http.StatusAccepted, code)
}

// func (h *handlerV1) Verify(c gin.Context){
// 	var (
// 		code = c.Param("code")
// 		email = c.Param("email")
// 		body models.RegisterUsermodel
// 	)

// 	userBody, err := h.redis.Get(email)
// 	if err != nil{
// 		c.JSON(http.StatusInternalServerError, gin.H{
// 			"error": err.Error(),
// 		})
// 		h.log.Error("error get from redis user body", l.Error(err))
// 		return
// 	}

// 	byteDate, err := redis.String(userBody)
// 	if err != nil{
// 		c.JSON(http.StatusInternalServerError, gin.H{
// 			"error": err.Error(),
// 		})
// 		h.log.Error("error set to redis body", l.Error(err))
// 		return
// 	}
// 	err :=json.Unmarshal([]byte(userBody,&body)

// }
