package api

import (
	_ "api_gateway/api/docs" //swag
	v1 "api_gateway/api/handlers/v1"
	"api_gateway/config"
	"api_gateway/pkg/logger"
	"api_gateway/services"
	"api_gateway/storage/repo"

	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
)

// Option ...
type Option struct {
	Conf           config.Config
	Logger         logger.Logger
	ServiceManager services.IServiceManager
	Redis          repo.RedisRepo
}

// New ...
func New(option Option) *gin.Engine {
	router := gin.New()

	router.Use(gin.Logger())
	router.Use(gin.Recovery())

	handlerV1 := v1.New(&v1.HandlerV1Config{
		Logger:         option.Logger,
		ServiceManager: option.ServiceManager,
		Cfg:            option.Conf,
		Redis:          option.Redis,
	})

	api := router.Group("/v1")
	api.POST("/users", handlerV1.CreateUser)
	api.GET("/users/:id", handlerV1.GetUser)
	api.POST("/users/update", handlerV1.UpdateUser)
	api.DELETE("/users/:id", handlerV1.DeleteUser)

	api.POST("/products/createproduct", handlerV1.CreateProduct)
	api.POST("/products/createcategory", handlerV1.CreateCategory)
	api.POST("/products/createtypes", handlerV1.CreateType)
	api.GET("/products/getproduct/:id", handlerV1.GetProductInfoByid)
	api.POST("/users/register", handlerV1.Register)

	url := ginSwagger.URL("swagger/doc.json")
	api.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))

	return router
}
